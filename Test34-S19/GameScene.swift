//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
// Test 

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var nextLevelButton:SKLabelNode!
    
    
    override func didMove(to view: SKView) {
        print("This is level 1")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as! SKLabelNode
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    var mouseStratingPosition:CGPoint = CGPoint(x:0,y:0)
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        let location = touch!.location(in:self)
        let mousePosition = touches.first?.location(in: self)
        let node = self.atPoint(location)
        
        print (mousePosition!.x)
        
        let stouch = self.atPoint(mousePosition!)
        
        if(stouch.name == "tree")
        {
            self.mouseStratingPosition = mousePosition!
        }
        else {
            self.mouseStratingPosition = CGPoint(x: 0, y: 0)
        }
        
        
        
        // MARK: Switch Levels
        if (node.name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level2")
            if (scene == nil) {
                print("Error loading level 2")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        
        func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
            
            //getting the mouse position on tap release
            let mousePosition = touches.first?.location(in: self)
            
            if(self.mouseStratingPosition.x != 0 && self.mouseStratingPosition.y != 0)
            {
                //gettin difference between strating and ending
                let diffX = mousePosition!.x - mouseStratingPosition.x
                let diffY = mousePosition!.y - mouseStratingPosition.y
                
                //make orange at mouse position and throw it in correct direction
                
                
                
            }}
    }
        
}
