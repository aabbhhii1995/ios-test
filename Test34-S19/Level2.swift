//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class Level2: SKScene {
    
    var nextLevelButton:SKLabelNode!
    let cat = SKSpriteNode(imageNamed: "left1")
    var lemming:SKNode?
    var ground:SKNode?
    var platform1:SKNode?
    
    
    override func didMove(to view: SKView) {
        print("Loaded level 2")
        
        self.platform1 = self.childNode(withName: "platform1")
        self.ground = self.childNode(withName: "ground")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as? SKLabelNode
        //self.makeCats()
        self.lemming = self.childNode(withName:"lemming")
        
        
        let a = -(self.size.width/2)
        let b = self.size.width/2

        let m1 = SKAction.moveBy(x: a, y: 0, duration: 3)
        // --- 1b. move right
        let m2 = SKAction.moveBy(x: b, y: 0, duration: 3)

        // 2. put actions into a sequence
        let sequence = SKAction.sequence([m1,m2])

        // 3. apply sequence to sprite
        self.lemming!.run(SKAction.repeatForever(sequence))
        
        
    }
    
   
    
    
    //let cat = SKSpriteNode(imageNamed: "left1")
    /*
    func makeCats() {
        // lets add some cats
        
        
        // generate a random (x,y) for the cat
        let randX = 500
        let randY = 1000
        
        cat.position = CGPoint(x:randX, y:randY)
        
        
        addChild(cat)
        
        print("Where is cat? \(randX), \(randY)")
        
    }*/

    func didBegin(_ contact: SKPhysicsContact) {
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        if (nodeA?.name == "lemming" && nodeB?.name == "ground") {
            print("Player reached exit")
            
        }
    }
    
    
    var timeOfLastUpdate:TimeInterval?
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
       
        
        
        
        /*
         
         
        if (timeOfLastUpdate == nil) {
                        timeOfLastUpdate = currentTime
                    }
                // print a message every 3 seconds
                var timePassed = (currentTime - timeOfLastUpdate!)
                if (timePassed >= 1.5) {
                    print("HERE IS A MESSAGE!")
                            timeOfLastUpdate = currentTime
                            // make a cat
                            self.makeCats()
                    }
        
*/
    }
    let platform = SKSpriteNode(imageNamed: "life_bg")
    func makePlatform(xPosition:CGFloat, yPosition:CGFloat) {
        
        // 1. create an orange sprite
        
        
        // 2. set initial position of orange to be same
        // as where mouse is clicked
        platform.position.x = xPosition;
        platform.position.y = yPosition;
        
        // 3. set physics for the orange
        // -- dyanmic = true
        // -- gravity = true
        // Both are true by default
        
        
        // 4. Add the orange to the scene
        addChild(platform)
        //self.platform.physicsBody?.categoryBitMask = 2
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // 1. detect where the person clicked
        let touch = touches.first!
        let mousePosition = touch.location(in:self)
        
        // 2. make an orange in the same position as mouse click
        self.makePlatform(
            xPosition:mousePosition.x,
            yPosition:mousePosition.y)
        
    
        
        //let location = touch!.location(in:self)
        let node = self.atPoint(mousePosition)
        
        
        
        
        
        // MARK: Switch Levels
        
        if (node.name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level3")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        
    }
}
